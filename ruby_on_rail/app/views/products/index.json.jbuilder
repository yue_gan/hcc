json.array!(@products) do |product|
  json.extract! product, :id, :title, :model, :p_speed, :m_speed, :memory, :brand, :chip_brand, :price_amazon, :price_newegg, :price_bestbuy
  json.url product_url(product, format: :json)
end

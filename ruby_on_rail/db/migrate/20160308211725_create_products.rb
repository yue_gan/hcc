class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :title
      t.string :model
      t.string :p_speed
      t.string :m_speed
      t.string :memory
      t.string :brand
      t.string :chip_brand
      t.float :price_amazon
      t.float :price_newegg
      t.float :price_bestbuy

      t.timestamps null: false
    end
  end
end

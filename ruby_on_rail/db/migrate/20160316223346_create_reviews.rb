class CreateReviews < ActiveRecord::Migration
  def change
    create_table :reviews do |t|
      t.string :model
      t.string :user_name
      t.text :content

      t.timestamps null: false
    end
  end
end

# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160316223346) do

  create_table "products", force: :cascade do |t|
    t.string   "title"
    t.string   "model"
    t.string   "p_speed"
    t.string   "m_speed"
    t.string   "memory"
    t.string   "brand"
    t.string   "chip_brand"
    t.float    "price_amazon"
    t.float    "price_newegg"
    t.float    "price_bestbuy"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.string   "image"
    t.float    "num_reviews"
    t.integer  "rate"
  end

  create_table "reviews", force: :cascade do |t|
    t.string   "model"
    t.string   "user_name"
    t.text     "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
